import { Component, h, State } from '@stencil/core';
import * as d3 from 'd3';

enum Size {
  FULL,
  CROP
}
@Component({
  tag: 'app-home',
  styleUrl: 'app-home.css',
})

export class AppHome {
  svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>;
  /*componentDidLoad(){
    
  }
*/

  componentWillLoad() {
    console.log('The component is about to be rendered');
  }

  /**
   * The component is loaded and has rendered.
   * 
   * Updating data in this event may cause the component to re-render.
   * 
   * Will only be called once
   */
  width: number = 600;
  height: number = 200;

  createSVG() {
   
    this.svg = d3.select("div#container")
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 " + this.width + " " + this.height)
      .classed("svg-content", true);



  }
  
  componentDidLoad() {
    console.log('The component has been rendered');
    this.createSVG();
    this.drawHorzLine();
    this.drawVertLine();
    this.drawSVG();



  }

  drawSVG() {
    var fullW = 36;//mm;
    //var fullH = 24;//mm
    var cropW = fullW / this.cropFactor;
    //var cropH = fullH / this.cropFactor;
    var sensor_width = fullW;
    var degrees = Math.atan(sensor_width / (2 * this.focalLength)) * (180 / Math.PI);
    this.drawUpperFovLine(degrees, Size.FULL);
    this.drawLowerFovLine(degrees, Size.FULL);

    var sensor_width = cropW;
    var degrees = Math.atan(sensor_width / (2 * this.focalLength)) * (180 / Math.PI);
    this.drawUpperFovLine(degrees, Size.CROP);
    this.drawLowerFovLine(degrees, Size.CROP);
  }

  drawHorzLine() {
    var x1 = this.width / 2;
    var y1 = 0;
    var x2 = this.width / 2
    var y2 = this.height - 0;
    this.svg.append('line')
      .attr('x1', x1)
      .attr('y1', y1)
      .attr('x2', x2)
      .attr('y2', y2)
      .attr('stroke', 'black');
  }
  drawVertLine() {
    var x1 = 0;
    var y1 = this.height / 2;
    var x2 = this.width - 0;
    var y2 = this.height / 2;
    this.svg.append('line')
      .attr('x1', x1)
      .attr('y1', y1)
      .attr('x2', x2)
      .attr('y2', y2)
      .attr('stroke', 'black');
  }

  getColor(size: Size) {
    let color = 'grey'

    if (size == Size.FULL) {
      color = 'red'
    }
    if (size == Size.CROP) {
      color = 'green'
    }
    return color;
  }
  drawUpperFovLine(degrees: number, size: Size) {
    var adjacent = this.width / 2
    var height = adjacent * Math.tan(this.deg2rad(degrees));

    console.log('drawUpperFovLine degrees', size, degrees);
    console.log('drawUpperFovLine height', size, height);

    var x1 = this.width / 2;
    var y1 = this.height / 2;
    var x2 = this.width;
    var y2 = (this.height / 2) + height;
    var line = this.svg.select('line.drawUpperFovLine' + size);

    if (line.size() == 0) {
      this.svg.append('line').attr('class', 'drawUpperFovLine' + size)
        .attr('x1', x1)
        .attr('y1', y1)
        .attr('x2', x2)
        .attr('y2', y2)
        .attr('stroke', this.getColor(size));
    } else {
      line

        .attr('x2', x2)
        .attr('y2', y2);
    }
  }

  drawLowerFovLine(degrees: number, size: Size) {
    var adjacent = this.width / 2
    var height = adjacent * Math.tan(this.deg2rad(degrees));

    console.log('drawLowerFovLine degrees', size, degrees);
    console.log('drawLowerFovLine height', size, height);

    var x1 = this.width / 2;
    var y1 = this.height / 2;
    var x2 = this.width;
    var y2 = (this.height / 2) - height;
    var line = this.svg.select('line.drawLowerFovLine' + size);

    if (line.empty()) {
      this.svg.append('line').attr('class', 'drawLowerFovLine' + size)
        .attr('x1', x1)
        .attr('y1', y1)
        .attr('x2', x2)
        .attr('y2', y2)
        .attr('stroke', this.getColor(size));
    } else {
      line

        .attr('x2', x2)
        .attr('y2', y2);
    }
  }

  deg2rad(degrees) {
    return degrees / (180 / Math.PI);
  }

  rad2deg(radians) {
    return radians * (180 / Math.PI);
  }
  /**
   * The component will update and re-render.
   *
   * Called multiple times throughout the life of the component as it updates.
   */
  componentWillUpdate() {
    console.log('The component will update');
  }

  /**
   * The component finished updating.
   *
   * Called after componentWillUpdate
   * 
   * Called multiple times throughout the life of the component as it updates.
   */
  componentDidUpdate() {
    console.log('The component did update');
  }

  /**
   * The component did unload and the element will be destroyed.
   */
  disconnectedCallback() {
    console.log('The view has been removed from the DOM');
  }

  focalLengthChanged(event: any) {
    this.focalLength = event.detail.value;
    this.drawSVG();


  }
  cropFactorChanged(event: any) {
    this.cropFactor = event.detail.value;
    this.drawSVG();


  }

  @State() focalLength = 35;
  @State() cropFactor = 1.62;
  render() {

    return [
      <ion-header>
        <ion-toolbar color="primary">
          <ion-title>Home</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">
        <p>
          Welcome to the Field of View playground for visualizing the area a lens can capture on a sensor in a camera
        </p>
        <ion-item>

          <ion-label>Crop Factor</ion-label>

          <ion-range  onIonChange={(event)=>this.cropFactorChanged(event)} id="cropFactor" pin={true} min={1.0} max={5.0} value={this.cropFactor} step={0.1}>
            <ion-label slot="start">0</ion-label>
            <ion-label slot="end">10</ion-label>
          </ion-range>
        </ion-item>

        <ion-item>
          <ion-label>Focal Length</ion-label>

          <ion-range  onIonChange={(event)=>this.focalLengthChanged(event)}  id="focalLength" pin={true} min={0} max={200} value={this.focalLength} step={5}>
            <ion-label slot="start">0</ion-label>
            <ion-label slot="end">200</ion-label>
          </ion-range>
        </ion-item>
        <h2>Field of View</h2>    
        <div id="container" class="svg-container" />
        <ion-button href="/profile/ionic" expand="block">
          Profile page
        </ion-button>
      </ion-content>,
    ];
  }
}
